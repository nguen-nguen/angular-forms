import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReminderPasswordComponent } from './reminder-password.component';

describe('ReminderPasswordComponent', () => {
  let component: ReminderPasswordComponent;
  let fixture: ComponentFixture<ReminderPasswordComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ReminderPasswordComponent]
    });
    fixture = TestBed.createComponent(ReminderPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
