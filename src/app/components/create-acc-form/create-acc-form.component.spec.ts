import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAccFormComponent } from './create-acc-form.component';

describe('CreateAccFormComponent', () => {
  let component: CreateAccFormComponent;
  let fixture: ComponentFixture<CreateAccFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateAccFormComponent]
    });
    fixture = TestBed.createComponent(CreateAccFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
