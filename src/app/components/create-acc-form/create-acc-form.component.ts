import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-acc-form',
  templateUrl: './create-acc-form.component.html',
  styleUrls: ['./create-acc-form.component.css']
})
export class CreateAccFormComponent implements OnInit{
  constructor() {
  }
  form = new FormGroup({
    username: new FormControl<string>('', [
      Validators.required,
      Validators.minLength(6)
    ]),
    email: new FormControl<string>('test@test.com', {

    })
  })
  handleSumit(){
    console.log('Hura')
  }
  ngOnInit() {
  }
}
