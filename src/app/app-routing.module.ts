import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {CreateAccFormComponent} from './components/create-acc-form/create-acc-form.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {ReminderPasswordComponent} from './components/reminder-password/reminder-password.component';

const routes: Routes = [{ path: "", component : LoginFormComponent}, {
  path : 'create', component: CreateAccFormComponent
},
  {
    path :'reminder', component : ReminderPasswordComponent
  },
  {
  path :'**', component : PageNotFoundComponent
}]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
